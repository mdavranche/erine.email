## Related issue

If your merge request closes an issue, use `Closes #<issue number>`. That will link the issue to the merge request, and vice-versa. That will also close the related issue while the merge request is merged - [Example](https://gitlab.com/mdavranche/erine.email/-/merge_requests/17).

If your merge request does not close an issue (but helps to do so), use `Relates to #<issue number>`. That will just link the issue to the merge request, and vice-versa.


This section is mandatory. If there is no related issue yet, create it first.


## Test that shows what the merge request does

If the related issue is a bug, it should contain a section named "Reproduce the problem" - [Example](https://gitlab.com/mdavranche/erine.email/-/issues/10). Run the same test again to show that the merge requests fixes the bug - [Example](https://gitlab.com/mdavranche/erine.email/-/merge_requests/17).

If the related issue is a feature request, show that the feature works.

This section is mandatory, except for code refactoring.


## Non regression tests

[Traviata](https://gitlab.com/mdavranche/traviata) is the functional test suite for erine.email. Use it to make your non regression tests. Tell us about the version of Traviata you used (always use the last version, but write it down as Traviata can change after your merge request). Also tell us about the email providers you used - [Example](https://gitlab.com/mdavranche/erine.email/-/merge_requests/17).

As you need several email test accounts and an on prem' installation of erine.email to run Traviata, this section is optional. If you didn't run the non regression tests, reviewers will do it.
