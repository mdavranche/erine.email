import datetime
import dropbox
import json
import logging
import os
import re
import requests
import subprocess
import sys


class backup:
    def __init__(self, filename, basename, extension):
        self.filename = filename
        r = re.match("{0}\.(.+)\.{1}(|\.encrypted)$".format(basename, extension), filename)
        if not r:
            self.valid = False
        else:
            try:
                self.datetime = datetime.datetime.strptime(r.groups()[0], "%Y-%m-%d_%H%M%S_%z")
            except:
                self.valid = False
            else:
                self.valid = True
                self.encrypted = r.groups()[1] == ".encrypted"


def runCommand(command, logOnFailure):
    """Execute command

    Exit 1 on failure
    Only show stdout and stderr on failure
    """
    try:
        completed = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except Exception as e:
        logging.error(e)
        sys.exit(1)
    else:
        if completed.returncode != 0:
            logging.error('"{0}" completed with a {1} return code'.format(command, completed.returncode))
            if completed.stdout:
                logging.info("stdout:\n{0}".format(completed.stdout.decode("utf-8")))
            if completed.stderr:
                logging.warn("stderr:\n{0}".format(completed.stderr.decode("utf-8")))
            logging.error(logOnFailure)
            sys.exit(1)


def upload(dbx, filename):
    """Upload a file on Dropbox

    Raise on error
    """
    with open(filename, "rb") as f:
        file_content = f.read()
    result = dbx.files_upload(
        file_content, "/{0}".format(os.path.basename(filename)), dropbox.files.WriteMode.overwrite, mute=True
    )
    logging.info("{0} uploaded on Dropbox".format(filename))


def _to_clean(filelist):
    """Browse filelist and return the backups to remove"""

    # Generate old_backups, a dictionary of the backups older than 15 days, with
    # year-month keys. Example:
    # {
    #   '2021-05': [<backup object 1>, <backup object 2>],
    #   '2021-06': [<backup object 3>, <backup object 4>]
    # }
    old_backups = {}
    for backup_file in filelist:
        if backup_file.datetime > datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=15):
            continue
        year_month = backup_file.datetime.strftime("%Y-%m")
        month_backups = old_backups.get(year_month, [])
        month_backups.append(backup_file)
        old_backups[year_month] = month_backups

    # Keep the first backup of the month, remove the others
    backups_to_clean = []
    for year_month in old_backups:
        month_backups = sorted(old_backups[year_month], key=lambda x: x.datetime)
        kept_backup = month_backups.pop(0)
        for backup_file in month_backups:
            backups_to_clean.append(backup_file)
    return backups_to_clean


def clean_dbx_folder(dbx, basename, extension):
    """Clean the Dropbox application root folder, containing the backups

    basename is the name of the backup file, without date, extension or encryption details
    extension is the original extension of the backup file (i.e. "sql")
    Abort the cleanup on error
    """
    try:
        result = dbx.files_list_folder("")
    except dropbox.exceptions.ApiError as e:
        logging.error("Folder listing failed: {0} ; Cleanup aborted".format(e))
        return
    if result.has_more:
        logging.warning('Unexpected "has_more" found ; The cleanup should be incomplete')
    backup_files = []
    unexpected_folders = 0
    unexpected_files = 0
    for entry in result.entries:
        if isinstance(entry, dropbox.files.FolderMetadata):
            unexpected_folders += 1
            continue
        backup_file = backup(entry.name, basename, extension)
        if backup_file.valid:
            backup_files.append(backup_file)
        else:
            unexpected_files += 1
    if unexpected_folders:
        logging.warning("{0} unexpected folders found on Dropbox ; They had been skipped".format(unexpected_folders))
    if unexpected_files:
        logging.warning("{0} unexpected files found on Dropbox ; They had been skipped".format(unexpected_files))
    for backup_file in _to_clean(backup_files):
        try:
            dbx.files_delete(path="/{0}".format(backup_file.filename))
            logging.info("{0} removed from Dropbox".format(backup_file.filename))
        except dropbox.exceptions.ApiError as e:
            logging.error("Can not remove {0}: {1} ; Cleanup aborted".format(backup_file.filename, e))
            return


def _get_dbx_refresh_token(app_key, app_secret):
    """Get interactively a Dropbox refresh_token"""
    print()
    print("Go to the following URL, and validate the authorization:")
    url = "https://www.dropbox.com/oauth2/authorize"
    print("{0}?client_id={1}&response_type=code&token_access_type=offline".format(url, app_key))
    print()
    print("Dropbox gave you a temporary code. I need it to generate a token.")
    code = input("Code: ")
    payload = {"code": code, "grant_type": "authorization_code", "client_id": app_key, "client_secret": app_secret}
    r = requests.post(
        url="https://api.dropboxapi.com/oauth2/token",
        data=payload,
    )
    if r.status_code != 200:
        print("ERROR - The Dropbox API returned {0}:".format(r.status_code))
        print(r.text)
        sys.exit(1)
    answer = json.loads(r.text)
    if "refresh_token" not in answer:
        print("ERROR - refresh_token not found on Dropbox API answer")
        print(r.text)
        sys.exit(1)
    _test_dbx_refresh_token(answer["refresh_token"], app_key, app_secret)
    return answer["refresh_token"]


def _test_dbx_refresh_token(refresh_token, app_key, app_secret):
    """Test the Dropbox refresh_token

    Exit 1 on failure
    """
    with dropbox.Dropbox(oauth2_refresh_token=refresh_token, app_key=app_key, app_secret=app_secret) as dbx:
        try:
            dbx.users_get_current_account()
        except dropbox.exceptions.AuthError:
            logging.error("Invalid Dropbox refresh_token")
            sys.exit(1)


def get_dbx_conf(config_file, interactive):
    """Return Dropbox's app_key, app_secret, refresh_token"""
    try:
        with open(config_file, "r") as f:
            configuration = f.readlines()
        configuration = json.loads("".join(configuration))
    except Exception as e:
        logging.error("Can not read {0}".format(config_file))
        logging.error("Fix this reading https://gitlab.com/mdavranche/erine.email/-/wikis/On-premises/Backups")
        sys.exit(1)
    conf_error = False
    for item in ["app_key", "app_secret"]:
        if item not in configuration:
            logging.error("{0} not found in {1}".format(item, config_file))
            conf_error = True
        if conf_error:
            logging.error("Fix this reading https://gitlab.com/mdavranche/erine.email/-/wikis/On-premises/Backups")
            sys.exit(1)
        app_key = configuration["app_key"]
        app_secret = configuration["app_secret"]
    if "refresh_token" not in configuration:
        if not interactive:
            logging.error(
                "refresh_token not found in {0} ; Use the --interactive option to set it up".format(config_file)
            )
            sys.exit(1)
        logging.warning("refresh_token not found in {0}".format(config_file))
        refresh_token = _get_dbx_refresh_token(app_key, app_secret)
        configuration["refresh_token"] = refresh_token
        with open(config_file, "w") as f:
            f.write(json.dumps(configuration))
        logging.info("refresh_token retrieved and saved")
    else:
        refresh_token = configuration["refresh_token"]
        _test_dbx_refresh_token(refresh_token, app_key, app_secret)
    return app_key, app_secret, refresh_token
