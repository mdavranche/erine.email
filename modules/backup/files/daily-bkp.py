#!/usr/bin/python3

import argparse
import datetime
import dropbox
import logging
import os
import sys

import erine_email_bkp


def main():

    # Get options
    parser = argparse.ArgumentParser(description="Make a backup, encrypt and upload it")
    parser.add_argument("--interactive", action="store_true", help="run the script in interactive mode")
    args = parser.parse_args()

    # Be sure /var/log/daily-bkp/daily-bkp.log exists and is accessible to backup user
    logging.root.setLevel(logging.INFO)
    logformat = logging.Formatter("%(asctime)s %(levelname)8s %(message)s", datefmt="%Y-%m-%d %H:%M:%S %Z")
    console = logging.StreamHandler(sys.stdout)
    console.setFormatter(logformat)
    logging.root.addHandler(console)
    file_handler = logging.FileHandler("/var/log/daily-bkp/daily-bkp.log")
    file_handler.setFormatter(logformat)
    logging.root.addHandler(file_handler)

    # Retrieve password
    try:
        with open("/var/backups/.mariadb.pwd", "r") as fd:
            password = fd.readline().strip()
    except Exception as e:
        logging.error(e)
        sys.exit(1)

    # Retrieve Dropbox configuration
    app_key, app_secret, refresh_token = erine_email_bkp.get_dbx_conf(
        "/var/backups/.config/daily-bkp/auth.json", args.interactive
    )

    # Set now, the current date and time
    # We use timezone.utc not because we're interested about UTC, but to have an object aware of the timezone (we could have
    # used any other timezone)
    now = datetime.datetime.now(datetime.timezone.utc)

    # Adjust "now" in local timezone
    now = now.astimezone()

    # Set dumpFile
    dumpFile = "/var/backups/spameater/spameater.{0}.sql".format(now.strftime("%Y-%m-%d_%H%M%S_%z"))

    # Check dumpFile
    # As it contains a timestamp accurate to the second, matching this condition probably means you're running this script
    # several times at the same time.
    if os.path.isfile(dumpFile):
        logging.error("{0} already exists".format(dumpFile))
        logging.error("Dump aborted")
        sys.exit(1)

    # Dump spameater database in dumpFile
    # We use --single-transaction as we exclusively use InnoDB
    logging.info("Dumping spameater database to {0}...".format(dumpFile))
    command = [
        "/usr/bin/mysqldump",
        "-u",
        "backup",
        "-p{0}".format(password),
        "--result-file={0}".format(dumpFile),
        "--single-transaction",
        "spameater",
    ]
    erine_email_bkp.runCommand(command, logOnFailure="Dump aborted")
    logging.info("Dump completed successfully")

    # Encrypt backup
    logging.info("Encrypting {0} to {0}.encrypted...".format(dumpFile))
    command = [
        "/usr/bin/gpg",
        "--output",
        "{0}.encrypted".format(dumpFile),
        "--default-recipient-self",
        "--encrypt",
        dumpFile,
    ]
    erine_email_bkp.runCommand(command, logOnFailure="Encryption aborted")
    os.remove(dumpFile)
    logging.info("Dump encrypted successfully")
    dumpFile = "{0}.encrypted".format(dumpFile)

    # Push the dump on Dropbox
    with dropbox.Dropbox(oauth2_refresh_token=refresh_token, app_key=app_key, app_secret=app_secret) as dbx:
        erine_email_bkp.upload(dbx, dumpFile)

    # Remove local dump
    os.remove(dumpFile)

    # Clean Dropbox folder
    erine_email_bkp.clean_dbx_folder(dbx, basename="spameater", extension="sql")


if __name__ == "__main__":
    main()
