# Install Apache HTTPd configuration for title and www.title sites
define web::install_site
{

  file { "/etc/apache2/sites-available/${title}.conf":
    ensure  => file,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('web/domain.conf.erb'),
    # We need the /etc/apache2/sites-available directory and the /etc/apache2 requirements
    require => File['/etc/apache2/sites-available'],
  }

  file { "/etc/apache2/sites-enabled/${title}.conf":
    ensure  => link,
    target  => "../sites-available/${title}.conf",
    notify  => Exec['reloadApache2'],
    # We need the destination file not to create the link, but so that the reloadApache2 makes sense
    require => File['/etc/apache2/sites-enabled', "/etc/apache2/sites-available/${title}.conf"],
  }

  file { "/etc/apache2/sites-available/${title}-ssl.conf":
    ensure  => file,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('web/domain-ssl.conf.erb'),
    # We need the /etc/apache2/sites-available directory and the /etc/apache2 requirements
    require => File['/etc/apache2/sites-available'],
  }

  file { "/etc/apache2/sites-enabled/${title}-ssl.conf":
    ensure  => link,
    target  => "../sites-available/${title}-ssl.conf",
    notify  => Exec['reloadApache2'],
    # We need the destination file not to create the link, but so that the reloadApache2 makes sense
    require => File['/etc/apache2/sites-enabled', "/etc/apache2/sites-available/${title}-ssl.conf"],
  }

  file { "/etc/apache2/sites-available/www.${title}.conf":
    ensure  => file,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('web/www.domain.conf.erb'),
    # We need the /etc/apache2/sites-available directory and the /etc/apache2 requirements
    require => File['/etc/apache2/sites-available'],
  }

  file { "/etc/apache2/sites-enabled/www.${title}.conf":
    ensure  => link,
    target  => "../sites-available/www.${title}.conf",
    notify  => Exec['reloadApache2'],
    # We need the destination file not to create the link, but so that the reloadApache2 makes sense
    require => File['/etc/apache2/sites-enabled', "/etc/apache2/sites-available/www.${title}.conf"],
  }

  file { "/etc/apache2/sites-available/www.${title}-ssl.conf":
    ensure  => file,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('web/www.domain-ssl.conf.erb'),
    # We need the /etc/apache2/sites-available directory and the /etc/apache2 requirements
    require => File['/etc/apache2/sites-available'],
  }

  file { "/etc/apache2/sites-enabled/www.${title}-ssl.conf":
    ensure  => link,
    target  => "../sites-available/www.${title}-ssl.conf",
    notify  => Exec['reloadApache2'],
    # We need the destination file not to create the link, but so that the reloadApache2 makes sense
    require => File['/etc/apache2/sites-enabled', "/etc/apache2/sites-available/www.${title}-ssl.conf"],
  }

}
