<div class="col-sm-6 col-sm-offset-3">
<?php
  $input = new \ptejada\uFlex\Collection($_GET);
  $hash = $input->c;
  if (!empty($hash))
  {

    // Password forgotten
    $email = $user->table->getRow(array('confirmation' => $hash))->mailAddress;
    if (empty($email))
    {
      redirect("/error");
    }

  }
  // "Loged-in and change password" case: data-success => /login => /disposableMails
  // "Not loged-in and password forgotten" case: data-success => /login
?>
  <form class="form-signin" method="post" action="actions/updatePassword.php" data-success="/login">
    <h3 class="form-signin-heading">Want a new password?</h3>
    <div class="form-group">
      <label for="password" class="sr-only">New password</label>
      <input name="password" type="password" class="form-control" placeholder="New password" required autofocus>
    </div>
    <div class="form-group">
      <label for="password2" class="sr-only">Confirm new password</label>
      <input name="password2" type="password" class="form-control" placeholder="Confirm new password" required>
    </div>
    <input name="c" type="hidden" value="<?php echo $hash; ?>">
    <div class="form-group text-center">
      <button type="submit" class="btn btn-primary btn-block">Change password</button>
      <br/>
      <a href="/account">Back to my account</a>
    </div>
  </form>
</div>
