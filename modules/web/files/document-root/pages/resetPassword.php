<?php
  if($user->isSigned()) redirect("/disposableMails");
?>
<div class="col-sm-6 col-sm-offset-3">
  <form class="form-signin" method="post" action="actions/resetPassword.php" data-success="/resetPasswordOK">
    <h3 class="form-signin-heading">Enter the email associated to your account</h3>
    <div class="form-group">
      <label for="mailAddress" class="sr-only">Email</label>
      <input name="mailAddress" type="text" required autofocus class="form-control" placeholder="Email">
    </div>
    <div class="form-group text-center">
      <button type="submit" class="btn btn-primary btn-block">Reset password</button>
      <br/>
      <a href="/login">Login</a>
    </div>
  </form>
</div>
