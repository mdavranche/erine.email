<?php
  if (!$user->isSigned()) { redirect("/login"); }
?>
<script>
function updateInfoDivAndTable() {
  $('#infoDiv').load('/disposableMailsDiv.php');
  $('#dmtable').DataTable({ "order": [[ 2, "desc" ]] });
}
document.addEventListener('DOMContentLoaded', updateInfoDivAndTable, false);
function toggleDisposableMail(disposableMail, presentStatus) {
  $.ajax({
    url: "/actions/toggleDisposableMail.php",
    type: "POST",
    data: {disposableMail: disposableMail, presentStatus: presentStatus},
    dataType: "json",
    success: function(data) {
      if (data.status == 'success'){
        $('#infoDiv').load('/disposableMailsDiv.php');
      }
      else{
        window.location = '/error';
      }
    },
    error: function() {
      window.location = '/error';
    }
  });
}
</script>

<div class="dmcontent">
  <div class="container marketing">
    <div class="panel panel-default">
      <div class="panel-heading">
        Global statistics
      </div>
      <div id="infoDiv" class="panel-body">
      </div>
    </div>
<?php
  $stmt = sqlquery($pdo, 'SELECT * FROM disposableMailAddress WHERE userID = :id', ['id' => $user->ID]);
?>
   <div class="panel panel-default panel-body">
      <div class="table-responsive">
        <table id="dmtable" class="table table-hover dmtable">
          <thead>
            <tr>
              <td>Enabled</td>
              <td>Mail address</td>
              <td>Creation date</td>
              <td>Sent</td>
              <td>Dropped</td>
              <td>Sent as</td>
            </tr>
          </thead>
          <tbody>
<?php
  while ($row = $stmt->fetch())
  {
?>
            <tr>
<?php
  echo '<td><input id="' . $row['mailAddress'] . '-checkbox" ';
  if ($row['enabled'] == 1)
  {
    echo "checked ";
  }
  echo 'data-toggle="toggle" type="checkbox" ';
  echo 'onchange="toggleDisposableMail(\'' . $row['mailAddress'] . '\', (this.checked) ? 0 : 1);">';

  /* Add an invisible text for column sorting */
  echo '<p style="display: none;">';
  echo ($row['enabled'] == 1 ? "enabled" : "disabled");
  echo '</p>';

  echo "</td>\n";
?>
              <td><?php echo $row['mailAddress']; ?></td>
              <td><?php echo $row['created']; ?></td>
              <td><?php echo $row['sent']; ?></td>
              <td><?php echo $row['dropped']; ?></td>
              <td><?php echo $row['sentAs']; ?></td>
            </tr>
<?php
}
?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="/static/css/bootstrap.datatables.net-1.10.15.css">
<script type="text/javascript" language="javascript" src="/static/js/jquery-datatables.net-1.10.15.min.js"></script>
<script type="text/javascript" language="javascript" src="/static/js/bootstrap.datatables.net-1.10.15.min.js"></script>
