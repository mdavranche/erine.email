# Install MariaDB server, client, and Python library
class database {

  exec { 'mkRootPwdFile':
    command => '/bin/mktemp -u XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX > /root/.mariadb.pwd',
    creates => '/root/.mariadb.pwd',
    notify  => Exec['mkRootPwdDeb'],
  }

  exec { 'mkRootPwdDeb':
    command     => '/bin/echo "mariadb-server mysql-server/root_password password `/bin/cat /root/.mariadb.pwd`" | debconf-set-selections',
    refreshonly => true,
    notify      => Exec['mkRootPwdDebAgain'],
  }

  exec { 'mkRootPwdDebAgain':
    command     => '/bin/echo "mariadb-server mysql-server/root_password_again password `/bin/cat /root/.mariadb.pwd`" | debconf-set-selections',
    refreshonly => true,
  }

  package { 'mariadb-server':
    ensure  => present,
    require => Exec['mkRootPwdFile'],
    notify  => Exec['rmRootPwdDeb'],
  }

  exec { 'rmRootPwdDeb':
    command     => '/bin/echo "mariadb-server mysql-server/root_password password" | debconf-set-selections',
    refreshonly => true,
    notify      => Exec['rmRootPwdDebAgain'],
  }

  exec { 'rmRootPwdDebAgain':
    command     => '/bin/echo "mariadb-server mysql-server/root_password_again password" | debconf-set-selections',
    refreshonly => true,
  }

  package { 'mariadb-client':
    ensure => present,
  }

  file { '/root/DCL-DDL':
    ensure => present,
    mode   => '0740',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/database/DCL-DDL.py',
  }

  file { '/root/ddl_queries.py':
    ensure => present,
    mode   => '0640',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/database/ddl_queries.py',
  }

  exec { 'DCL-DDL':
    command => '/root/DCL-DDL',
    require => [
      User['spameater'],
      File['/etc/erine-email.conf'],
      File['/home/www-data'],
      File['/root/DCL-DDL'],
      File['/root/ddl_queries.py'],
      Exec['pip3_SQLAlchemy'],
    ]
  }

  file { '/etc/erine-email.conf':
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/database/erine-email.conf',
  }

}
