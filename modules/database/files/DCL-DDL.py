#!/usr/bin/env python3

import configparser
import os
import pwd
import re
import secrets
import stat
import string
import sqlalchemy
import sys

from ddl_queries import ddl_queries


def create_user(db_connection, db_user, db_host, sys_user):
    """Create a user in DB, and write password on their home directory"""
    password_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
    password = "".join(secrets.choice(password_chars) for i in range(40))
    pw = pwd.getpwnam(sys_user)
    password_filename = os.path.join(pw.pw_dir, ".mariadb.pwd")
    with open(password_filename, "w") as pwd_file:
        pwd_file.write(password)
    os.chown(password_filename, pw.pw_uid, pw.pw_gid)
    os.chmod(password_filename, stat.S_IRUSR)
    query = "CREATE USER :user@:host IDENTIFIED BY :password"
    db_connection.execute(sqlalchemy.text(query), user=db_user, host=db_host, password=password)


def get_versions(db_connection, database):
    """Return the DDL versions of the tables

    db_connection: sqlalchemy.engine.Connection to information_schema
    database: string

    Exit 1 on unexpected format
    """
    query = "SELECT table_name, table_comment FROM tables WHERE table_schema=:table_schema"
    results = db_connection.execute(sqlalchemy.text(query), table_schema=database)
    issue_found = False
    versions = {}
    for row in results.all():
        table = row._mapping["table_name"]
        version = row._mapping["table_comment"]
        if version and not re.match(r"^v\d{3}$", version):
            print("ERROR - Unknown version for the {0} table: {1}".format(table, version))
            issue_found = True
        versions[table] = version
    if issue_found:
        sys.exit(1)
    return versions


def set_version(db_connection, table, version):
    """Set the DDL version of a table"""
    query = "ALTER TABLE `{0}` COMMENT=:version".format(table)
    db_connection.execute(sqlalchemy.text(query), table=table, version=version)
    print("INFO - {0} table DDL upgraded to {1}".format(table, version))


def handle_ddl_v001(versions, db_connection):
    """Handle DDL changes made on v001

    Return if something changed
    """
    change = False
    for table in ["user", "disposableMailAddress", "message", "replyAddress"]:
        if not versions[table]:
            set_version(db_connection, table, "v001")
            change = True
    return change


def handle_ddl_v002(versions, db_connection):
    """Handle DDL changes made on v002

    Return if something changed
    """
    change = False
    if versions["disposableMailAddress"] == "v001":
        set_version(db_connection, "disposableMailAddress", "v001 to v002, doing...")
        query = "ALTER TABLE `disposableMailAddress` DROP COLUMN `remaining`"
        db_connection.execute(sqlalchemy.text(query))
        set_version(db_connection, "disposableMailAddress", "v002")
        change = True
    for table in ["user", "message", "replyAddress"]:
        if versions[table] == "v001":
            set_version(db_connection, table, "v002")
            change = True
    return change


def handle_ddl_v003(versions, db_connection):
    """Handle DDL changes made on v003

    Return if something changed
    """
    change = False
    if versions["disposableMailAddress"] == versions["user"] == "v002":
        set_version(db_connection, "disposableMailAddress", "v002 to v003, doing...")
        set_version(db_connection, "user", "v002 to v003, doing...")
        for query in [
            "ALTER TABLE `disposableMailAddress` DROP FOREIGN KEY `disposableMailAddress_ibfk_1`",
            "ALTER TABLE `user` MODIFY `ID` integer NOT NULL AUTO_INCREMENT",
            "ALTER TABLE `disposableMailAddress` MODIFY `userID` integer NOT NULL",
            "ALTER TABLE `disposableMailAddress` ADD CONSTRAINT `disposableMailAddress_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`ID`)",
            "ALTER TABLE `user` MODIFY reserved tinyint NOT NULL DEFAULT 0",
            "ALTER TABLE `user` MODIFY activated tinyint NOT NULL DEFAULT 0",
            "ALTER TABLE `user` MODIFY `registrationDate` integer DEFAULT NULL",
            "ALTER TABLE `user` MODIFY `lastLogin` integer DEFAULT NULL",
            "ALTER TABLE `disposableMailAddress` MODIFY `enabled` tinyint NOT NULL DEFAULT 1",
            "ALTER TABLE `disposableMailAddress` MODIFY `sent` integer NOT NULL DEFAULT 0",
            "ALTER TABLE `disposableMailAddress` MODIFY `sentAs` integer NOT NULL DEFAULT 0",
        ]:
            db_connection.execute(sqlalchemy.text(query))
        set_version(db_connection, "disposableMailAddress", "v003")
        set_version(db_connection, "user", "v003")
        change = True
    if versions["message"] == "v002":
        query = "ALTER TABLE `message` MODIFY `id` integer NOT NULL AUTO_INCREMENT"
        db_connection.execute(sqlalchemy.text(query))
        set_version(db_connection, "message", "v003")
        change = True
    if versions["replyAddress"] == "v002":
        set_version(db_connection, "replyAddress", "v003")
        change = True
    return change


def main():

    # Retrieve DBMS configuration
    try:
        config = configparser.RawConfigParser()
        config.read_file(open("/etc/erine-email.conf"))
        host = config.get("dbms", "host", fallback="localhost")
        port = int(config.get("dbms", "port", fallback=3306))
        database = config.get("dbms", "database", fallback="spameater")
    except Exception as e:
        print("WARNING - Problem retrieving DBMS configuration: {0}".format(str(e)))
        print("WARNING - Falling back to spameater database on localhost:3306.")
        host = "localhost"
        port = 3306
        database = "spameater"
    with open("/root/.mariadb.pwd", "r") as pwd_file:
        password = pwd_file.readline().strip()

    db_engine = sqlalchemy.create_engine(
        "mysql+mysqldb://root:{0}@{1}:{2}?charset=utf8mb4".format(password, host, port)
    )
    db_connection = db_engine.connect().execution_options(autocommit=True)
    if not os.path.isfile(os.path.join(pwd.getpwnam("backup").pw_dir, ".mariadb.pwd")):
        create_user(db_connection, db_user="backup", db_host="localhost", sys_user="backup")
    if not os.path.isfile(os.path.join(pwd.getpwnam("mnet").pw_dir, ".mariadb.pwd")):
        create_user(db_connection, db_user="mnet", db_host="localhost", sys_user="mnet")
    if not os.path.isfile(os.path.join(pwd.getpwnam("spameater").pw_dir, ".mariadb.pwd")):
        create_user(db_connection, db_user="spameater", db_host="%", sys_user="spameater")
    if not os.path.isfile(os.path.join(pwd.getpwnam("www-data").pw_dir, ".mariadb.pwd")):
        create_user(db_connection, db_user="www", db_host="%", sys_user="www-data")

    # WARNING, do NOT replace the below collation by a case sensitive one. It would lead to unexpected behaviors
    query = "CREATE DATABASE IF NOT EXISTS `{0}` DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_general_ci"
    db_connection.execute(sqlalchemy.text(query.format(database)))

    db_engine = sqlalchemy.create_engine(
        "mysql+mysqldb://root:{0}@{1}:{2}/{3}?charset=utf8mb4".format(password, host, port, database)
    )
    db_connection = db_engine.connect().execution_options(autocommit=True)
    for query in ddl_queries("mysql", db_connection):
        db_connection.execute(sqlalchemy.text(query))
    for query in [
        "GRANT SELECT ON `{0}`.* TO 'backup'@'localhost'",
        "GRANT SELECT ON `{0}`.`disposableMailAddress` TO 'mnet'@'localhost'",
        "GRANT SELECT, DELETE ON `{0}`.`message` TO 'mnet'@'localhost'",
        "GRANT SELECT, UPDATE, INSERT ON `{0}`.`sender` TO 'mnet'@'localhost'",
        "GRANT SELECT, DELETE ON `{0}`.`user` TO 'mnet'@'localhost'",
        "GRANT SELECT ON `{0}`.`user` TO 'spameater'@'%'",
        "GRANT SELECT, UPDATE, INSERT ON `{0}`.`disposableMailAddress` TO 'spameater'@'%'",
        "GRANT SELECT, INSERT ON `{0}`.`message` TO 'spameater'@'%'",
        "GRANT SELECT, INSERT ON `{0}`.`replyAddress` TO 'spameater'@'%'",
        "GRANT SELECT, UPDATE, INSERT ON `{0}`.`user` TO 'www'@'%'",
        "GRANT SELECT, UPDATE ON `{0}`.`disposableMailAddress` TO 'www'@'%'",
        "GRANT SELECT ON `{0}`.`message` TO 'www'@'%'",
    ]:
        db_connection.execute(sqlalchemy.text(query.format(database)))

    # Handle table versions
    if_connection = sqlalchemy.create_engine(
        "mysql+mysqldb://root:{0}@{1}:{2}/information_schema?charset=utf8mb4".format(password, host, port)
    ).connect()
    versions = get_versions(if_connection, database)
    if handle_ddl_v001(versions, db_connection):
        versions = get_versions(if_connection, database)
    if handle_ddl_v002(versions, db_connection):
        versions = get_versions(if_connection, database)
    handle_ddl_v003(versions, db_connection)
    db_connection.close()


if __name__ == "__main__":
    main()
