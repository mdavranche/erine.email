#!/usr/bin/env python3

import re
import sqlalchemy


def index_exists(engine, db_connection, table, columns):
    if engine == "sqlite":
        query = "SELECT name FROM sqlite_master WHERE type='table'"
    else:
        query = "SHOW TABLES"
    results = db_connection.execute(sqlalchemy.text(query))
    table_exists = False
    for row in results.all():
        if row[0] == table:
            table_exists = True
            break
    if not table_exists:
        return False
    if engine == "sqlite":
        query = "PRAGMA index_list({0})".format(table)
        results = db_connection.execute(sqlalchemy.text(query))
        for row in results.all():
            query = "PRAGMA index_info({0})".format(row[1])
            results2 = db_connection.execute(sqlalchemy.text(query))
            index_columns = set()
            for row2 in results2.all():
                index_columns.add(row2[2])
            if index_columns == columns:
                return True
    else:
        query = "SHOW INDEXES FROM `{0}`".format(table)
        results = db_connection.execute(sqlalchemy.text(query))
        indexes = {}
        for row in results.all():
            if row._mapping["Index_type"] != "BTREE":
                continue
            if row._mapping["Key_name"] not in indexes:
                indexes[row._mapping["Key_name"]] = set([row._mapping["Column_name"]])
            else:
                indexes[row._mapping["Key_name"]].add(row._mapping["Column_name"])
        for index in indexes:
            if indexes[index] == columns:
                return True
    return False


def ddl_queries(engine, db_connection):
    queries = []
    for query in [
        (
            "CREATE TABLE IF NOT EXISTS `user` ("
            "`ID` integer NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            "`username` varchar(15) NOT NULL UNIQUE,"
            "`reserved` tinyint NOT NULL DEFAULT 0,"
            "`firstName` varchar(15) NOT NULL,"
            "`lastName` varchar(15) NOT NULL,"
            "`password` varchar(40) NOT NULL,"
            "`mailAddress` varchar(254) NOT NULL,"
            "`activated` tinyint NOT NULL DEFAULT 0,"
            "`confirmation` char(40) NOT NULL DEFAULT '',"
            "`registrationDate` integer NOT NULL,"
            "`lastLogin` integer NOT NULL DEFAULT 0"
            ") ENGINE=InnoDB COMMENT='v003'"
        ),
        (
            "CREATE TABLE IF NOT EXISTS `disposableMailAddress` ("
            "`mailAddress` varchar(254) NOT NULL PRIMARY KEY,"
            "`userID` integer NOT NULL,"
            "`created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
            "`enabled` tinyint NOT NULL DEFAULT 1,"
            "`sent` integer NOT NULL DEFAULT 0,"
            "`dropped` integer NOT NULL DEFAULT 0,"
            "`sentAs` integer NOT NULL DEFAULT 0,"
            "`comment` varchar(140) DEFAULT NULL,"
            "FOREIGN KEY (`userID`) REFERENCES `user` (`ID`)"
            ") ENGINE=InnoDB COMMENT='v003'"
        ),
        (
            "CREATE TABLE IF NOT EXISTS `message` ("
            "`id` integer NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            "`disposableMailAddress` varchar(254) DEFAULT NULL,"
            "`messageId` varchar(998) NOT NULL,"
            "`subject` varchar(998) NOT NULL,"
            "`from` varchar(2048) NOT NULL,"
            "`rcptTo` varchar(2048) NOT NULL,"
            "`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
            "`status` ENUM('sent', 'dropped', 'sentAs', 'looped'),"
            "FOREIGN KEY (`disposableMailAddress`) REFERENCES `disposableMailAddress` (`mailAddress`)"
            ") ENGINE=InnoDB COMMENT='v003'"
        ),
        (
            "CREATE TABLE IF NOT EXISTS `replyAddress` ("
            "`mailAddress` varchar(254) NOT NULL UNIQUE,"
            "`disposableMailAddress` varchar(254) NOT NULL,"
            "`foreignAddress` varchar(254) NOT NULL,"
            "UNIQUE (`disposableMailAddress`,`foreignAddress`),"
            "FOREIGN KEY (`disposableMailAddress`) REFERENCES `disposableMailAddress` (`mailAddress`)"
            ") ENGINE=InnoDB COMMENT='v003'"
        ),
        (
            # We should have added a foreign key constraint, but MySQL doesn't want both composite unique & foreign key
            # TODO - Add foreign key constraint when we'll use PostgreSQL
            "CREATE TABLE IF NOT EXISTS `sender` ("
            "`disposableMailAddress` varchar(254) NOT NULL,"
            "`from` varchar(2048) NOT NULL,"
            "`sent` integer NOT NULL DEFAULT 0,"
            "`dropped` integer NOT NULL DEFAULT 0,"
            "`first` timestamp NOT NULL,"
            "`last` timestamp NOT NULL,"
            "UNIQUE (`disposableMailAddress`,`from`)"
            ") ENGINE=InnoDB COMMENT='v003'"
        ),
    ]:
        if engine == "sqlite":
            query = query.replace("PRIMARY KEY AUTO_INCREMENT,", "PRIMARY KEY AUTOINCREMENT,")
            query = query.replace(") ENGINE=InnoDB", ")")
            r = re.search(r"^(.+)`(.+)`\s+ENUM\((.+?)\)(.+)$", query)
            if r:
                groups = r.groups()
                query = "{0}`{1}` TEXT CHECK ( `{1}` IN ({2}) ){3}".format(groups[0], groups[1], groups[2], groups[3])
            r = re.search(r"^(.+) COMMENT='(.+)'$", query)
            if r:
                groups = r.groups()
                query = "{0}\n-- {1}".format(groups[0], groups[1])
        queries.append(query)
    if engine == "sqlite":
        queries.append("PRAGMA foreign_keys = ON")
    if not index_exists(engine, db_connection, "disposableMailAddress", set(["userID"])):
        queries.append("CREATE INDEX `disposableMailAddress_idx1` ON `disposableMailAddress`(`userID`)")
    if not index_exists(engine, db_connection, "message", set(["disposableMailAddress"])):
        queries.append("CREATE INDEX `message_idx1` ON `message`(`disposableMailAddress`)"),
    if not index_exists(engine, db_connection, "message", set(["messageId"])):
        queries.append("CREATE INDEX `message_idx2` ON `message`(`messageId`)"),
    if not index_exists(engine, db_connection, "sender", set(["disposableMailAddress", "from"])):
        queries.append(
            "CREATE INDEX `sender_idx1` ON `sender`(`disposableMailAddress`{0}, `from`{0})".format(
                "" if engine == "sqlite" else "(10)"
            )
        ),
    return queries
