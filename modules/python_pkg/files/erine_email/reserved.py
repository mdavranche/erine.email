import re
import sqlalchemy

from erine_email.classic_reserved import ClassicReserved
from erine_email.common import reply_reserved_regex, BounceException
from functools import lru_cache


class Reserved(ClassicReserved):
    @property
    @lru_cache(maxsize=None)
    def outbound_recipient(self):

        # r necessarily exists: the email() function already checked that
        r = re.search(reply_reserved_regex, self.inbound_recipient)

        # The record necessarily exists: the check_usage_security() function already checked that
        query = sqlalchemy.text("SELECT `mailAddress` FROM `user` WHERE `username` = :username")
        results = self.db_connection.execute(query, username=r.group(1))
        row = results.one_or_none()
        self.create_disposable_address(disposable_address=self.inbound_recipient, username=r.group(1))
        return row._mapping["mailAddress"]

    def check_usage_security(self):

        # r necessarily exists: the email() function already checked that
        r = re.search(reply_reserved_regex, self.inbound_recipient)

        query = sqlalchemy.text("SELECT `reserved`, `activated` FROM `user` WHERE `username` = :username")
        results = self.db_connection.execute(query, username=r.group(1))
        row = results.one_or_none()
        if not row:
            raise BounceException("Reserved - Unknown user name: {0}".format(r.group(1)))
        if not row._mapping["reserved"]:
            raise BounceException(
                "Reserved - Incorrect user usage: {0} exists, but not as a reserved user".format(r.group(1))
            )
        if not row._mapping["activated"]:
            raise BounceException("Reserved - The {0} user had not been activated".format(r.group(1)))
