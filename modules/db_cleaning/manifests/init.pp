# Install script and its requirements to clean the database
class db_cleaning {

  group { 'mnet':
    ensure => present,
    gid    => '143',
  }

  # M. Net, French Canada's Mr. Clean
  user { 'mnet':
    ensure     => present,
    uid        => '143',
    gid        => 'mnet',
    shell      => '/usr/sbin/nologin',
    home       => '/home/mnet',
    managehome => true,
    password   => '*',
    require    => Group['mnet'],
  }

  file { '/usr/bin/clean-db':
    ensure  => file,
    mode    => '0744',
    owner   => 'mnet',
    group   => 'mnet',
    source  => 'puppet:///modules/db_cleaning/clean-db.py',
    require => [
      User['mnet'],
      Group['mnet'],
    ],
  }

  file { '/var/spool/cron/crontabs/mnet':
    ensure => present,
    mode   => '0600',
    owner  => 'mnet',
    group  => 'crontab',
    source => 'puppet:///modules/db_cleaning/mnet.cron',
  }

  file { '/var/log/clean-db':
    ensure => directory,
    mode   => '0755',
    owner  => 'mnet',
    group  => 'mnet',
  }

  file { '/etc/logrotate.d/clean-db':
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/db_cleaning/clean-db.logrotate',
  }

}
