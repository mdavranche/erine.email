#!/usr/bin/env python3

import configparser
import datetime
import logging
import sqlalchemy
import sys


def extract_data(lasttime, disposable_mail_address, _from):
    """Extract data (count of sent and dropped messages, first and last date) from the 'message' table"""
    query = sqlalchemy.text(
        "SELECT count(*) AS `c` "
        "FROM `message` "
        "WHERE `date` < :lasttime AND `disposableMailAddress` = :disposable_mail_address AND `from` = :_from AND `status` = :status"
    )
    sent = db_connection.execute(
        query,
        lasttime=lasttime,
        disposable_mail_address=disposable_mail_address,
        _from=_from,
        status="sent",
    ).fetchone()["c"]
    dropped = db_connection.execute(
        query,
        lasttime=lasttime,
        disposable_mail_address=disposable_mail_address,
        _from=_from,
        status="dropped",
    ).fetchone()["c"]
    query = sqlalchemy.text(
        "SELECT min(date) AS min, max(date) AS max "
        "FROM `message` "
        "WHERE `date` < :lasttime AND `disposableMailAddress` = :disposable_mail_address AND `from` = :_from"
    )
    row = db_connection.execute(
        query, lasttime=lasttime, disposable_mail_address=disposable_mail_address, _from=_from
    ).fetchone()
    first = row["min"]
    last = row["max"]
    return sent, dropped, first, last


def upsert(disposable_mail_address, _from, sent, dropped, first, last):
    """Upsert on 'sender' table, and return 'updated' or 'created'"""
    query = sqlalchemy.text(
        "SELECT `sent`, `dropped`, `first`, `last` "
        "FROM `sender` "
        "WHERE `disposableMailAddress`=:disposable_mail_address AND `from`=:_from"
    )
    row = db_connection.execute(query, disposable_mail_address=disposable_mail_address, _from=_from).one_or_none()
    if row:
        query = sqlalchemy.text(
            "UPDATE `sender` "
            "SET `sent`=:sent, `dropped`=:dropped, `first`=:first, `last`=:last "
            "WHERE `disposableMailAddress`=:disposable_mail_address AND `from`=:_from"
        )
        db_connection.execute(
            query,
            sent=row["sent"] + sent,
            dropped=row["dropped"] + dropped,
            first=min(row["first"], first),
            last=max(row["last"], last),
            disposable_mail_address=disposable_mail_address,
            _from=_from,
        )
        return "updated"
    else:
        query = sqlalchemy.text(
            "INSERT INTO `sender` (`disposableMailAddress`, `from`, `sent`, `dropped`, `first`, `last`) "
            "VALUES (:disposable_mail_address, :_from, :sent, :dropped, :first, :last)"
        )
        db_connection.execute(
            query,
            disposable_mail_address=disposable_mail_address,
            _from=_from,
            sent=sent,
            dropped=dropped,
            first=first,
            last=last,
        )
        return "created"


def clean_message():
    """Clean the 'message' table, and update the 'sender' table accordingly"""

    # Set retention_days to 1 year and count all messages
    retention_days = 365
    query = sqlalchemy.text("SELECT count(*) AS c FROM `message`")
    total_count = db_connection.execute(query).fetchone()["c"]

    # Skip if cleaning had been done less than a day ago, or if there is nothing to clean
    lasttime = datetime.datetime.strftime(
        datetime.datetime.now() - datetime.timedelta(days=retention_days + 1), "%Y-%m-%d"
    )
    query = sqlalchemy.text("SELECT count(*) AS c FROM `message` WHERE `date` < :lasttime")
    count = db_connection.execute(query, lasttime=lasttime).fetchone()["c"]
    if not count:
        return

    # Display stats
    lasttime = datetime.datetime.strftime(datetime.datetime.now() - datetime.timedelta(days=retention_days), "%Y-%m-%d")
    query = sqlalchemy.text("SELECT count(*) AS c FROM `message` WHERE `date` < :lasttime")
    count = db_connection.execute(query, lasttime=lasttime).fetchone()["c"]
    logging.info("Total messages: {0} ; Messages older than {1} days: {2}".format(total_count, retention_days, count))
    query = sqlalchemy.text(
        "SELECT count(*) AS `c` FROM ("
        "SELECT `disposableMailAddress`, `from` FROM `message` WHERE `date` < :lasttime GROUP BY `disposableMailAddress`, `from`"
        ") AS a"
    )
    count = db_connection.execute(query, lasttime=lasttime).fetchone()["c"]
    logging.info("In those old messages, there are {0} distinct (disposableMailAddress, from)".format(count))

    # Clean messages
    n = 1
    stats = {"created": 0, "updated": 0, "deleted": 0}
    while n <= count:
        db_transaction = db_connection.begin()
        query = sqlalchemy.text(
            "SELECT `disposableMailAddress`, `from` FROM `message` WHERE `date` < :lasttime LIMIT 1"
        )
        row = db_connection.execute(query, lasttime=lasttime).fetchone()
        disposable_mail_address = row["disposableMailAddress"]
        _from = row["from"]
        sent, dropped, first, last = extract_data(lasttime, disposable_mail_address, _from)
        if sent or dropped:
            upsert_status = upsert(disposable_mail_address, _from, sent, dropped, first, last)
            stats[upsert_status] += 1
        query = sqlalchemy.text(
            "DELETE FROM `message` "
            "WHERE `date` < :lasttime AND `disposableMailAddress` = :disposable_mail_address AND `from` = :_from"
        )
        result = db_connection.execute(
            query,
            lasttime=lasttime,
            disposable_mail_address=disposable_mail_address,
            _from=_from,
        )
        stats["deleted"] += result.rowcount
        db_transaction.commit()
        n += 1
    if stats["created"]:
        logging.info("{0} 'sender' rows created".format(stats["created"]))
    if stats["updated"]:
        logging.info("{0} 'sender' rows updated".format(stats["updated"]))
    if stats["deleted"]:
        logging.info("{0} 'message' rows deleted".format(stats["deleted"]))


def clean_user():
    """Clean the 'user' table"""

    # Set limit to 1 day ago, EPOCH format
    limit = (datetime.datetime.now() - datetime.timedelta(days=1)).timestamp()

    # Set page_size
    page_size = 100

    # Clean users
    query = sqlalchemy.text(
        "SELECT `user`.`ID`, `user`.`username`, `user`.`mailAddress`, `user`.`registrationDate` "
        "FROM `user` "
        "LEFT JOIN `disposableMailAddress` ON `disposableMailAddress`.`userID` = `user`.`ID` "
        "WHERE `user`.`activated` = 0 "
        "AND `user`.`reserved` = 0 "
        "AND `user`.`registrationDate` < :limit "
        "AND `disposableMailAddress`.`userID` IS NULL "
        "ORDER BY `user`.`registrationDate` "
        "LIMIT :page_size"
    )
    users_to_delete = db_connection.execute(query, limit=limit, page_size=page_size)
    for row in users_to_delete.all():
        registration_date = datetime.datetime.fromtimestamp(row._mapping["registrationDate"])
        user_info = "ID: {0} ; Username: {1} ; Mail address: {2} ; Registration date: {3}".format(
            row._mapping["ID"],
            row._mapping["username"],
            row._mapping["mailAddress"],
            datetime.datetime.strftime(registration_date, "%Y-%m-%d %H:%M:%S"),
        )
        db_transaction = db_connection.begin()
        query = sqlalchemy.text("DELETE FROM `user` WHERE `ID` = :_id")
        result = db_connection.execute(query, _id=row._mapping["ID"])
        if result.rowcount != 1:
            db_transaction.rollback()
            logging.error(
                "Unexpected {0} row count while deleting non-activated {1}".format(result.rowcount, user_info)
            )
            sys.exit(1)
        else:
            db_transaction.commit()
            logging.info("Deleted not activated user {0}".format(user_info))
    if users_to_delete.rowcount == page_size:
        clean_user()


def main():

    # Be sure /var/log/clean-db/clean-db.log exists and is accessible to mnet user
    logging.root.setLevel(logging.INFO)
    logformat = logging.Formatter("%(asctime)s %(levelname)8s %(message)s", datefmt="%Y-%m-%d %H:%M:%S %Z")
    console = logging.StreamHandler(sys.stdout)
    console.setFormatter(logformat)
    logging.root.addHandler(console)
    file_handler = logging.FileHandler("/var/log/clean-db/clean-db.log")
    file_handler.setFormatter(logformat)
    logging.root.addHandler(file_handler)

    # Retrieve DBMS configuration
    try:
        config = configparser.RawConfigParser()
        config.read_file(open("/etc/erine-email.conf"))
        host = config.get("dbms", "host", fallback="localhost")
        port = int(config.get("dbms", "port", fallback=3306))
        database = config.get("dbms", "database", fallback="spameater")
    except Exception as e:
        logging.warning("Problem retrieving DBMS configuration: {0}".format(str(e)))
        logging.warning("Falling back to spameater database on localhost:3306.")
        host = "localhost"
        port = 3306
        database = "spameater"
    with open("/home/mnet/.mariadb.pwd", "r") as pwd_file:
        password = pwd_file.readline().strip()

    # Connect to the database
    db_engine = sqlalchemy.create_engine(
        "mysql+mysqldb://mnet:{0}@{1}:{2}/{3}?charset=utf8mb4".format(password, host, port, database)
    )
    global db_connection
    db_connection = db_engine.connect().execution_options(autocommit=False)

    # Clean tables
    clean_message()
    clean_user()

    # Close the connection to the database
    db_connection.close()


if __name__ == "__main__":
    main()
