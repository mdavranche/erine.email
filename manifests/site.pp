node default
{
  include base
  include backup
  include database
  include db_cleaning
  include letsencrypt
  include logrotate
  include postfix
  include python_pkg
  include python_requirements
  include web
  include wwwdata
}
